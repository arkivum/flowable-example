package com.arkivum;

import org.flowable.engine.IdentityService;
import org.flowable.engine.RuntimeService;
import org.flowable.engine.TaskService;
import org.flowable.task.api.Task;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service
@Transactional
public class FlowableBusinessService {

    @Autowired
    private RuntimeService runtimeService;

    @Autowired
    private TaskService taskService;

    @Autowired
    private IdentityService identityService;

    @Autowired
    private PersonRepository personRepository;

    public void startProcess(String assignee) {

        Person person = personRepository.findByUsername(assignee);

        System.out.println("Process starting...");
        Map<String, Object> variables = new HashMap<>();
        variables.put("person", person);
        runtimeService.startProcessInstanceByKey("HelloWorld", variables);
    }

    public void hello() {
        System.out.println("******** Hello World ********");
    }

    public void goodbye() {
        System.out.println("******** Goodbye World ********");
    }

    @Transactional
    public List<Task> getTasks(String assignee) {
        return taskService.createTaskQuery().taskAssignee(assignee).list();
    }

    public void approveTask(String approved, String assignee) {
        List<Task> tasks = taskService.createTaskQuery().taskAssignee(assignee).list();
        if (!tasks.isEmpty()) {
            Task task = tasks.get(0);
            String id = task.getId();
            Map<String, Object> variables = taskService.getVariables(id);
            variables.put("approved", approved.toLowerCase().equals("y"));
            taskService.complete(id, variables);
        } else {
            System.out.println("You have no pending tasks.");
        }
    }

    public void createDemoUsers() {
        if (!personRepository.findAll().iterator().hasNext()) {
            personRepository.save(new Person("alexnel", "Alex", "Nel", new Date()));
            personRepository.save(new Person("testUser", "Test", "User", new Date()));
        }
    }
}