package com.arkivum;

import java.util.Arrays;

import org.flowable.app.spring.SpringAppEngineConfiguration;
import org.flowable.spring.boot.EngineConfigurationConfigurer;
import org.flowable.variable.service.impl.types.JPAEntityListVariableType;
import org.flowable.variable.service.impl.types.JPAEntityVariableType;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@AutoConfigureAfter(SpringAppEngineConfiguration.class)
public class AppEngineConfiguration {

    @Bean
    @ConditionalOnClass(SpringAppEngineConfiguration.class)
    public EngineConfigurationConfigurer<SpringAppEngineConfiguration> customAppEngineConfigurationConfigurer() {

        return appEngineConfiguration -> appEngineConfiguration
                .setCustomPostVariableTypes(Arrays.asList(new JPAEntityVariableType(), new JPAEntityListVariableType()));
    }
}
